import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';

import {
	OAuth2Strategy,
	Profile,
} from 'passport-google-oauth';

import { UnsuccessfulLoginException } from '../common/exceptions/unsuccessful-login.exception';
import { ConfigService } from '../config/config.service';
import { AuthService, AuthProvider } from './auth.service';


@Injectable()
export class GoogleStrategy extends PassportStrategy(OAuth2Strategy, AuthProvider.GOOGLE) {

	constructor(
		private readonly authService: AuthService,
		private readonly configService: ConfigService
	) {
		super({
			clientID: configService.googleOAuthClientId,
			clientSecret: configService.googleOAuthClientSecret,
			callbackURL: `http://localhost:${configService.serverPort}/auth/${AuthProvider.GOOGLE}/callback`,
			scope: ['profile', 'email']
		});
	  }
	
	async validate(accessToken: string, refreshToken: string, profile: Profile) {
		const user = await this.authService.validateGoogleUser(
			accessToken,
			refreshToken,
			profile,
		);
		if (user == null) {
			throw new UnsuccessfulLoginException();
		}

		return user;
	}
}