import { Strategy } from 'passport-local';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { AuthService } from './auth.service';

import { UserExistsException } from '../common/exceptions/user-exists.exception';

@Injectable()
export class LocalRegisterStrategy extends PassportStrategy(Strategy, 'local-register') {
  constructor(private readonly authService: AuthService) {
    super({
      usernameField: 'email',
      passwordField: 'password',
    });
  }

  async validate(email: string, password: string): Promise<any> {
    const user = await this.authService.createUser(email, password);
    if (!user) {
      throw new UserExistsException();
    }
    return user;
  }
}
