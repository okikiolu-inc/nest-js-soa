import { Injectable } from '@nestjs/common';
import { Profile as GoogleProfile } from 'passport-google-oauth';
import { Profile as FacebookProfile } from 'passport-facebook';
import { Profile as GithubProfile } from 'passport-github';

import { UsersService } from '../users/users.service';
import { User } from '../users/user.entity';


export enum AuthProvider {
  GOOGLE = 'google',
  FACEBOOK = 'facebook',
  GITHUB = 'github'
}

@Injectable()
export class AuthService {
  constructor(
    private readonly usersService: UsersService,
  ) {}

  async validateUser(email: string, password: string): Promise<any> {
    return await this.usersService.validateLocalOne(email, password);
  }

  async createUser(email: string, password: string): Promise<any> {
    return await this.usersService.createLocalOne(email, password);
  }

  public async validateGoogleUser(token: string, refreshToken: string, profile: GoogleProfile): Promise<User | undefined> {
    return await this.usersService.findOrCreateGoogleUser(token, refreshToken, profile);
  }

  public async validateFacebookUser(token: string, refreshToken: string, profile:FacebookProfile): Promise<User | undefined> {
    return await this.usersService.findOrCreateFacebookUser(token, refreshToken, profile);
  }

  public async validateGithubUser(token: string, refreshToken: string, profile:GithubProfile): Promise<User | undefined> {
    return await this.usersService.findOrCreateGithubUser(token, refreshToken, profile);
  }
}
