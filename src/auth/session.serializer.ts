import { PassportSerializer } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';

import { AuthService } from './auth.service';
import { UsersService } from '../users/users.service';

import { User } from '../users/user.entity';
@Injectable()
export class SessionSerializer extends PassportSerializer {
  constructor(
    private readonly authService: AuthService,
    private readonly usersService: UsersService,
  ) {
    super();
  }

  serializeUser(user: User, done: (err: Error, user: any) => void): any {
    done(null, user.id);
  }
  async deserializeUser(userId: number, done: (err: Error, payload: User) => void): Promise<any> {
    return await this.usersService.findOneById(userId)
      .then(user => done(null, user))
      .catch(error => done(error, null));
  }
}