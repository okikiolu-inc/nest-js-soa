import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';

import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';

import { ConfigService } from '../config/config.service';

import { UsersModule } from '../users/users.module';

// local auth
import { LocalStrategy } from './local.strategy';
import { LocalRegisterStrategy } from './local-register.strategy';
import { SessionSerializer } from './session.serializer';

// import { JwtModule } from '@nestjs/jwt';
// import { jwtConstants } from './constants';
// import { JwtStrategy } from './jwt.strategy';

// 3rd Party Oath Authentication
import { GoogleStrategy } from './google.strategy';
import { FacebookStrategy } from './facebook.strategy';
import { GithubStrategy } from './github.strategy';


@Module({
  imports: [
    UsersModule,
    PassportModule,
  ],
  providers: [
    AuthService,
    {
      provide: ConfigService,
      useValue: new ConfigService(`${process.env.NODE_ENV ? process.env.NODE_ENV : 'production'}.env`),
    },
    LocalStrategy, // simply by importing them will register them to passport (under the hood it calls `passport.use(...)`)
    LocalRegisterStrategy,
    SessionSerializer,
    GoogleStrategy,
    FacebookStrategy,
    GithubStrategy,
  ],
  exports: [AuthService],
  controllers: [AuthController],
})
export class AuthModule {}
