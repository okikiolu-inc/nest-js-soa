import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';

import {
  Strategy,
  Profile,
} from 'passport-github';

import { UnsuccessfulLoginException } from '../common/exceptions/unsuccessful-login.exception';
import { ConfigService } from '../config/config.service';
import { AuthService, AuthProvider } from './auth.service';

/*
  VerifyFunction function means a different thing in this strategy
  vs the google strategy, so I just wrote a custom interface that mimics
  googles VerifyFunction with a different name
  see : https://github.com/DefinitelyTyped/DefinitelyTyped/blob/master/types/passport-facebook/index.d.ts for facebook and
  see : https://github.com/DefinitelyTyped/DefinitelyTyped/blob/master/types/passport-google-oauth/index.d.ts for google
  and compare the two VerifyFunction Declaration
*/
interface DoneFunction {
  (error: any, user?: any, info?: any): void
}

@Injectable()
export class GithubStrategy extends PassportStrategy(Strategy, AuthProvider.GITHUB) {
	constructor(
		private readonly authService: AuthService,
		private readonly configService: ConfigService
	) {
		super({
			clientID: configService.githubOAuthClientId,
			clientSecret: configService.githubOAuthClientSecret,
			callbackURL: `http://localhost:${configService.serverPort}/auth/${AuthProvider.GITHUB}/callback`,
			scope: ['read:user', 'user:email'],
		});
	  }
	
	async validate(accessToken: string, refreshToken: string, profile: Profile) {
		const user = await this.authService.validateGithubUser(
			accessToken,
			refreshToken,
			profile,
		);
		if (user == null) {
			throw new UnsuccessfulLoginException();
		}

		return user;
	}
}