import {
    Controller,
    Get,
    Post,
    Res,
    Req,
    UseGuards,
    UseFilters,
} from '@nestjs/common';

import { Response } from 'express';

import {
    AuthService,
    AuthProvider,
} from './auth.service';

import { LoginGuard } from './guard/login.guard';

import { RegisterGuard } from './guard/register.guard';

import { AuthenticatedGuard } from './guard/authenticated.guard';

// social auth guards
import { GoogleAuthGuard } from './guard/google.guard';
import { GithubAuthGuard } from './guard/github.guard';
import { FacebookAuthGuard } from './guard/facebook.guard';

import { GoogleStrategy } from './google.strategy';

import { FacebookStrategy } from './facebook.strategy';

import { GithubStrategy } from './github.strategy';

import { LocalRegisterStrategy } from './local-register.strategy';

import { AuthExceptionFilter } from '../common/filters/auth-exception.filter';

@Controller('auth')
@UseFilters(new AuthExceptionFilter())
export class AuthController {
  constructor(
      private readonly authService: AuthService,
      private readonly localRegisterStrategy: LocalRegisterStrategy,
      private readonly googleStrategy: GoogleStrategy,
      private readonly facebookStrategy: FacebookStrategy,
      private readonly githubStrategy: GithubStrategy,
    ) {}

    // this 'guard' is not really a guard
    // it is for logging in using passport local
    @UseGuards(LoginGuard)
    @Post('login')
    async login(@Req() req, @Res() res: Response) {
        res.redirect('/');
    }

    @UseGuards(RegisterGuard)
    @Post('register')
    async register(@Res() res: Response) {
        res.redirect('/');
    }

    @UseGuards(AuthenticatedGuard)
    @Post('change_password')
    async changePassword(@Req() req, @Res() res: Response) {
        // res.redirect('/');
    }

    @UseGuards(AuthenticatedGuard)
    @Post('reset_password')
    async resetPassword(@Req() req, @Res() res: Response) {
        // res.redirect('/');
    }

    @Get('logout')
    async logout(@Req() req, @Res() res: Response) {
        req.logout();
        res.redirect('/');
    }

    @Get(`${AuthProvider.GOOGLE}/login`)
	@UseGuards(GoogleAuthGuard)
	public googleLogin(): void {}

    @Get(`${AuthProvider.GOOGLE}/callback`)
	@UseGuards(GoogleAuthGuard)    
	public googleLoginCallback(@Res() res: Response) {
        res.redirect('/');
    }
    
    @Get(`${AuthProvider.FACEBOOK}/login`)
	@UseGuards(FacebookAuthGuard)
	public facebookLogin(): void {}

    @Get(`${AuthProvider.FACEBOOK}/callback`)
	@UseGuards(FacebookAuthGuard)    
	public facebookLoginCallback(@Res() res: Response) {
        res.redirect('/');
    }
    
    @Get(`${AuthProvider.GITHUB}/login`)
	@UseGuards(GithubAuthGuard)
	public githubLogin(): void {}

    @Get(`${AuthProvider.GITHUB}/callback`)
    @UseGuards(GithubAuthGuard)
	public githubLoginCallback(@Res() res: Response) {
        res.redirect('/');
	}
}
