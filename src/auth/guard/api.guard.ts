
import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Observable } from 'rxjs';
import { Request } from 'express';

@Injectable()
export class APIGuard implements CanActivate {
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const request = context.switchToHttp().getRequest<Request>();
    // *****************************
    // WE WILL DO REQUEST VALIDATION
    // *****************************
    return this.validateRequest(request);
  }


  private validateRequest(request:  Request) : boolean {
    return false;
  }
}