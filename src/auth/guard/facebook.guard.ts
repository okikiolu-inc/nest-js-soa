import {
    Injectable,
    ExecutionContext,
    UnauthorizedException
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

import { User } from '../../users/user.entity';
import {
    AuthProvider,
} from '../auth.service';

@Injectable()
export class FacebookAuthGuard extends AuthGuard(AuthProvider.FACEBOOK) {
	public async canActivate(context: ExecutionContext) {
		const result = (await super.canActivate(context)) as boolean;
		const request = context.switchToHttp().getRequest();
		await super.logIn(request);
		return result;
	}

	public handleRequest<U extends User>(err: Error, user: U, info?: string): U {
		if (err != null) {
			throw err;
		}

		if (user == null) {
			throw new UnauthorizedException();
		}

		return user;
	}
}