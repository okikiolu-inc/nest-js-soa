import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import {
  Strategy,
  Profile,
  StrategyOptionWithRequest,
} from 'passport-facebook';
import { Request } from 'express';

import { UnsuccessfulLoginException } from '../common/exceptions/unsuccessful-login.exception';
import { ConfigService } from '../config/config.service';
import { AuthService, AuthProvider } from './auth.service';

/*
  VerifyFunction function means a different thing in this strategy
  vs the google strategy, so I just wrote a custom interface that mimics
  googles VerifyFunction with a different name
  see : https://github.com/DefinitelyTyped/DefinitelyTyped/blob/master/types/passport-facebook/index.d.ts for facebook and
  see : https://github.com/DefinitelyTyped/DefinitelyTyped/blob/master/types/passport-google-oauth/index.d.ts for google
  and compare the two VerifyFunction Declaration
*/
interface DoneFunction {
  (error: any, user?: any, info?: any): void
}

@Injectable()
export class FacebookStrategy extends PassportStrategy(Strategy, AuthProvider.FACEBOOK) {
	/*
	public constructor(authService: AuthService, configService: ConfigService) {
		const options:StrategyOptionWithRequest  =  {
			clientID: configService.facebookOAuthClientId,
			clientSecret: configService.facebookOAuthClientSecret,
			callbackURL: `http://localhost:${configService.serverPort}/auth/${AuthProvider.FACEBOOK}/callback`,
      enableProof: true,
      passReqToCallback: true,
			profileFields: ['profile', 'email', 'displayName']
		};

		super(
			options,
			async (
				req: Request,
				accessToken: string,
				refreshToken: string,
				profile: Profile,
				done: DoneFunction
			): Promise<void> => {
				console.log('FACEBOOK AUTH TEST');
				console.log('accessToken:::', accessToken);
				console.log('refreshToken:::', refreshToken);
				console.log('profile:::', profile);

				const user = await authService.validateFacebookUser(accessToken, refreshToken, profile);
				if (user == null) {
					done(new UnauthorizedException());
				}

				done(undefined, user);
			}
		);
	}
	*/

	constructor(
		private readonly authService: AuthService,
		private readonly configService: ConfigService
	) {
		super({
			clientID: configService.facebookOAuthClientId,
			clientSecret: configService.facebookOAuthClientSecret,
			callbackURL: `http://localhost:${configService.serverPort}/auth/${AuthProvider.FACEBOOK}/callback`,
			enableProof: true,
			passReqToCallback: true,
			profileFields: ['profile', 'email', 'displayName'],
		});
	  }
	
	async validate(accessToken: string, refreshToken: string, profile: Profile) {
		console.log('FACEBOOK AUTH TEST');
		console.log('accessToken:::', accessToken);
		console.log('refreshToken:::', refreshToken);
		console.log('profile:::', profile);

		const user = await this.authService.validateFacebookUser(
			accessToken,
			refreshToken,
			profile,
		);
		if (user == null) {
			throw new UnsuccessfulLoginException();
		}

		return user;
	}
}