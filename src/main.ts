import { NestFactory } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';

// security
import * as helmet from 'helmet';
import * as cookieParser from 'cookie-parser';
import * as bodyParser from 'body-parser';
import * as csurf from 'csurf';
import * as rateLimit from 'express-rate-limit';
import * as compression from 'compression'
import * as exphbs from 'express-handlebars';

import * as express_session from 'express-session';
import flash = require('connect-flash');
import * as passport from 'passport';

import { join } from 'path';

import * as hbsHelpers from '../views/helpers/helpers';

import { init, initQueueValues } from './common/libs/rabbitmq';

import { AppModule } from './app.module';
import { HttpExceptionFilter } from './common/filters/http-exception.filter';

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);

  app.useStaticAssets(join(__dirname, '..', 'public'));
  app.useGlobalFilters(new HttpExceptionFilter());
  // NEW VIEW ENGINE METHOD
  app.engine('.hbs', exphbs({
    defaultLayout: "default",
    extname: '.hbs',
    helpers: hbsHelpers, //only need this
    partialsDir: join(__dirname, '..', 'views', 'partials'),
    layoutsDir: join(__dirname, '..', 'views', 'layouts'),
  }));
  app.set('view engine', '.hbs');

  app.use(helmet());
  app.use(
    rateLimit({
      windowMs: 15 * 60 * 1000, // 15 minutes
      max: 100, // limit each IP to 100 requests per windowMs
    }),
  );

  const configService = app.get('ConfigService');
  const SESSION_SECRET: string = configService.sessionSecret;

  initQueueValues(
    configService.rabbitMQHost,
    configService.rabbitMQPort,
    configService.rabbitMQUsername,
    configService.rabbitMQPassword,
  );

  try {
    init();
  }
  catch (err) {
    console.error('[AMQP Error]:', err);
  }

  app.use(
    express_session({
      secret: SESSION_SECRET,
      resave: true,
      saveUninitialized: false,
      cookie: {
        maxAge: 1 * 60 * 60 * 1000, // 1 hour cookie age
      }
    }),
  );
  app.use(passport.initialize());
  app.use(passport.session());
  // use connect-flash for flash messages stored in session
  app.use(flash());
  // flush previous connect-flash messages and keep only the recent one
  app.use((req, res, next) => {
    if (req.session && req.session.flash && req.session.flash.length > 0) {
      req.session.flash = [];
    }
    next();
  });


  // For a HIGH-TRAFFIC website in production, the best way to put compression
  // is to implement it at a reverse proxy level. In that case, you do not
  // need to use compression middleware below.
  app.use(compression());

  // need this for _csrf parsing in forms
  app.use(bodyParser.urlencoded({
    extended: true
  }));

  // csurf uses cookieParser
  app.use(cookieParser());

  app.use(csurf({ cookie: true }));

  // Make the token available to all views
  app.use(function (req, res, next){
    res.locals._csrf = req.csrfToken();
    next();
  });
  
  app.enableCors();

  const port = 3000;

  await app.listen(port);

  console.log(`listening on port: ${port}...`);
}
bootstrap();
