import * as dotenv from 'dotenv';
import * as Joi from '@hapi/joi';
import * as fs from 'fs';
import * as path from 'path';

export interface EnvConfig {
  [key: string]: string;
}

export class ConfigService {
  private readonly envConfig: EnvConfig;

  constructor(filePath: string) {
    const config = dotenv.parse(fs.readFileSync(filePath));
    this.envConfig = this.validateInput(config);
  }

  // For each config property, we have to add a getter function.
  // e.g
  /*
  get isApiAuthEnabled(): boolean {
    return Boolean(this.envConfig.API_AUTH_ENABLED);
  }
  */

  get isProduction(): boolean {
    return Boolean(this.envConfig.IS_PRODUCTION);
  }

  get serverPort(): number {
    return Number(this.envConfig.SERVER_PORT);
  }

  get jwtSecret(): string {
    return String(this.envConfig.JWT_SECRET);
  }

  get sessionSecret(): string {
    return String(this.envConfig.SESSION_SECRET);
  }

  get googleOAuthClientId(): string {
    return String(this.envConfig.GOOGLE_OAUTH_CLIENT_ID);
  }

  get googleOAuthClientSecret(): string {
    return String(this.envConfig.GOOGLE_OAUTH_CLIENT_ID_SECRET);
  }

  get facebookOAuthClientId(): string {
    return String(this.envConfig.FACEBOOK_OAUTH_CLIENT_ID);
  }

  get facebookOAuthClientSecret(): string {
    return String(this.envConfig.FACEBOOK_OAUTH_CLIENT_ID_SECRET);
  }

  get githubOAuthClientId(): string {
    return String(this.envConfig.GITHUB_OAUTH_CLIENT_ID);
  }

  get githubOAuthClientSecret(): string {
    return String(this.envConfig.GITHUB_OAUTH_CLIENT_ID_SECRET);
  }

  get databaseType(): string {
    return String(this.envConfig.DATABASE_TYPE);
  }

  get databaseHost(): string {
    return String(this.envConfig.DATABASE_HOST);
  }

  get databaseName(): string {
    return String(this.envConfig.DATABASE_NAME);
  }

  get databasePort(): number {
    return Number(this.envConfig.DATABASE_PORT);
  }

  get databaseUsername(): string {
    return String(this.envConfig.DATABASE_USERNAME);
  }

  get databasePassword(): string {
    return String(this.envConfig.DATABASE_PASSWORD);
  }

  get tempUploadDestination(): string {
    return String(path.join(this.envConfig.BASE_UPLOAD_DESTINATION, this.envConfig.TEMP_UPLOAD_DESTINATION));
  }

  get uploadDestination(): string {
    return String(path.join(this.envConfig.BASE_UPLOAD_DESTINATION, this.envConfig.UPLOAD_DESTINATION));
  }

  get multerUploadDestination(): string {
    return String(path.join(this.envConfig.BASE_UPLOAD_DESTINATION, this.envConfig.MULTER_UPLOAD_DESTINATION));
  }

  get rabbitMQUsername(): string {
    return String(this.envConfig.RABBIT_MQ_USERNAME);
  }

  get rabbitMQPassword(): string {
    return String(this.envConfig.RABBIT_MQ_PASSWORD);
  }

  get rabbitMQHost(): string {
    return String(this.envConfig.RABBIT_MQ_HOST);
  }

  get rabbitMQPort(): number {
    return Number(this.envConfig.RABBIT_MQ_PORT);
  }

  get rabbitMQBackOff(): number {
    return Number(this.envConfig.RABBIT_MQ_BACK_OFF);
  }

  get rabbitMQMaxBackOff(): number {
    return Number(this.envConfig.RABBIT_MQ_MAX_BACK_OFF);
  }

  get(configname: string): string {
    if (this.envConfig[configname] == null) {
      return '';
    }

    return String(this.envConfig[configname]);
  }



  /**
   * Ensures all needed variables are set, and returns the validated JavaScript object
   * including the applied default values.
   */
  private validateInput(envConfig: EnvConfig): EnvConfig {
    const envVarsSchema: Joi.ObjectSchema = Joi.object({
      NODE_ENV: Joi.string()
        .valid(['development', 'production', 'test', 'provision'])
        .default('development'),
      HOST_NAME: Joi.string(),
      SERVER_PORT: Joi.number().default(3000), 
      IS_PRODUCTION: Joi.boolean(),
      JWT_SECRET: Joi.string(),
      SESSION_SECRET: Joi.string(),
      GOOGLE_OAUTH_CLIENT_ID: Joi.string(),
      GOOGLE_OAUTH_CLIENT_ID_SECRET: Joi.string(),
      FACEBOOK_OAUTH_CLIENT_ID: Joi.string(),
      FACEBOOK_OAUTH_CLIENT_ID_SECRET: Joi.string(),
      GITHUB_OAUTH_CLIENT_ID: Joi.string(),
      GITHUB_OAUTH_CLIENT_ID_SECRET: Joi.string(),
      DATABASE_TYPE: Joi.string(),
      DATABASE_HOST: Joi.string(),
      DATABASE_NAME: Joi.string(),
      DATABASE_PORT: Joi.string(),
      DATABASE_USERNAME: Joi.string(),
      DATABASE_PASSWORD: Joi.string().default('').allow('').allow(null),
      BASE_UPLOAD_DESTINATION: Joi.string().default('/tmp/resumable_js'),
      UPLOAD_DESTINATION: Joi.string(),
      MULTER_UPLOAD_DESTINATION: Joi.string(),
      TEMP_UPLOAD_DESTINATION: Joi.string(),
      RABBIT_MQ_HOST: Joi.string().default('localhost'),
      RABBIT_MQ_PORT: Joi.number().default(5672),
      RABBIT_MQ_USERNAME: Joi.string().default('guest'),
      RABBIT_MQ_PASSWORD: Joi.string().default('guest'),
      RABBIT_MQ_HEARTBEAT: Joi.string().default('60'),
      RABBIT_MQ_BACK_OFF: Joi.number().default(5000),
      RABBIT_MQ_MAX_BACK_OFF: Joi.number().default(25000),
    });

    const { error, value: validatedEnvConfig } = Joi.validate(
      envConfig,
      envVarsSchema,
    );
    if (error) {
      throw new Error(`Config validation error: ${error.message}`);
    }
    return validatedEnvConfig;
  }
}
