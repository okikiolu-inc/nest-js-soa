import { Module } from '@nestjs/common';
import { TypeOrmModule, TypeOrmModuleOptions } from '@nestjs/typeorm';
import { Connection } from 'typeorm';
import { join } from 'path';
import { AccessControlModule } from 'nest-access-control';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from './config/config.module';
import { ConfigService } from './config/config.service';
import { roles } from './app.roles';
import { EmailService } from './email/email.service';
import { EmailModule } from './email/email.module';
import { BillingModule } from './billing/billing.module';
import { MetricsModule } from './metrics/metrics.module';
import { LogModule } from './log/log.module';
import { MindModule } from './mind/mind.module';

@Module({
  imports: [
    AccessControlModule.forRoles(roles),
    AuthModule,
    UsersModule,
    ConfigModule,
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => {
        // TypeOrmModuleOptions
        return {
          type: configService.databaseType,
          host: configService.databaseHost,
          port: configService.databasePort,
          username: configService.databaseUsername,
          password: configService.databasePassword,
          database: configService.databaseName,
          entities: [join(__dirname, '**/**.entity{.ts,.js}')],
          synchronize: false,
        } as TypeOrmModuleOptions;
      },
      inject: [ConfigService],
    }),
    EmailModule,
    BillingModule,
    MetricsModule,
    LogModule,
    MindModule,
  ],
  controllers: [AppController],
  providers: [
    AppService,
    {
      provide: ConfigService,
      useValue: new ConfigService(`${process.env.NODE_ENV ? process.env.NODE_ENV : 'production'}.env`),
    },
    EmailService,
  ],
})
export class AppModule {
  constructor(private readonly connection: Connection) {}
}
