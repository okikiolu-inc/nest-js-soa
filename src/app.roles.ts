import { RolesBuilder } from 'nest-access-control';

// EXAMPLE: defining roles
export enum AppRoles {
    USER_TEST_GRANT_ALL = 'USER_TEST_GRANT_ALL',
    ADMIN_TEST_GRANT_ALL = 'ADMIN_TEST_GRANT_ALL',
};

export const roles: RolesBuilder = new RolesBuilder();

// EXAMPLE: granting permissions 
roles
    .grant(AppRoles.USER_TEST_GRANT_ALL)
    .createOwn('servicename') // equal to .createOwn('video', ['*'])
    .deleteOwn('servicename')
    .readAny('serviceName')
    .grant(AppRoles.ADMIN_TEST_GRANT_ALL)
    .extend(AppRoles.USER_TEST_GRANT_ALL)
    .updateAny('serviceName', ['servicePropertyName'])
    .deleteAny('serviceName');