import * as fs from 'fs';
import * as path from 'path';
import { Request, Response } from 'express';

import { User } from '../../users/user.entity';

type GetCallback = (status: string, chunkFilename: string, filename: string, identifier: string) => void;
type PostCallback = (status: string, filename: string, original_filename: string, identifier: string) => void;

export enum ResumableStatus {
    Done = 'Done',
    Invalid = 'Invalid',
    NonResumable = 'NonResumable',
    Valid = 'Valid',
    PartlyDone = 'PartlyDone',
    Found = 'Found',
    NotFound = 'NotFound',
}

// interface so typescript knows multer exists
export interface MulterFile {
    fieldname: string,
    originalname: string,
    encoding: string,
    mimetype: string,
    destination: string,
    filename: string,
    path: string,
    size: number
}

export class Resumable {

    private temporaryFolder: string;
    private uploadFolder: string;
    private maxFileSize: number;
    private fileParameterName: string;

    constructor(temporaryFolder: string, uploadFolder: string) {
        this.temporaryFolder = temporaryFolder;
        this.uploadFolder = uploadFolder;
        this.maxFileSize = null;
        this.fileParameterName = 'file';

        try {
            fs.mkdirSync(this.temporaryFolder);
        }
        catch(e){}
    }

    private cleanIdentifier (identifier: string): string {
        return identifier.replace(/^0-9A-Za-z_-/img, '');
    }

    private getChunksFolder (userid: number, identifier: string): string {
        // Clean up the identifier
        identifier = this.cleanIdentifier(identifier);
        return path.join(this.temporaryFolder, userid.toString(), identifier);
    }

    private getChunkFilename (userid: number, chunkNumber: number, identifier: string): string {
        // Clean up the identifier
        identifier = this.cleanIdentifier(identifier);
        // What would the file name be?
        return path.join(this.temporaryFolder, userid.toString(), identifier, './resumable-'+identifier+'.'+chunkNumber);
    }

    private validateRequest (chunkNumber: number, chunkSize: number, totalSize: number, _identifier: string, filename: string, fileSize?: number): string {
        // Clean up the identifier
        const identifier = this.cleanIdentifier(_identifier);

        // Check if the request is sane
        if (chunkNumber==0 || chunkSize==0 || totalSize==0 || identifier.length==0 || filename.length==0) {
        return ResumableStatus.NonResumable;
        }
        let numberOfChunks = Math.max(Math.floor(totalSize/(chunkSize*1.0)), 1);
        if (chunkNumber>numberOfChunks) {
            return 'invalid_resumable_request1';
        }

        // Is the file too big?
        if(this.maxFileSize && totalSize>this.maxFileSize) {
            return 'invalid_resumable_request2';
        }

        if(typeof(fileSize)!='undefined') {
            if(chunkNumber<numberOfChunks && fileSize!=chunkSize) {
                // The chunk in the POST request isn't the correct size
                return 'invalid_resumable_request3';
            }
            if(numberOfChunks>1 && chunkNumber==numberOfChunks && fileSize!=((totalSize%chunkSize)+chunkSize)) {
                // The chunks in the POST is the last one, and the fil is not the correct size
                return 'invalid_resumable_request4';
            }
            if(numberOfChunks==1 && fileSize!=totalSize) {
                // The file is only a single chunk, and the data size does not fit
                return 'invalid_resumable_request5';
            }
        }

        return ResumableStatus.Valid;
    }

    private createFileFromChunks (userid: number, identifier: string, original_filename: string, total_files: number) {
        const temp_dir = this.getChunksFolder(userid, identifier);
        const upload_dir = path.join(this.uploadFolder, userid.toString());
        const final_filename = path.join(upload_dir, original_filename);

        this.mkDirByPathSync(upload_dir);

        // clean the file if it exists
        // incase of any partial writes
        // **** will need to deal with cases where
        // **** people upload different files with the
        // **** same original name
        fs.closeSync(fs.openSync(final_filename, 'w'));

        for (let part = 1; part <= total_files; part++) {
            const chunkFilename = this.getChunkFilename(userid, part, identifier);
            const chunk = fs.readFileSync(chunkFilename);
            fs.writeFileSync(
                final_filename,
                chunk,
                {
                    flag: 'a',
                }
            );
        }

        const temp_dir_marked = temp_dir + '_UNUSED';
        fs.renameSync(temp_dir, temp_dir_marked);
        this.rrmdir(temp_dir_marked);
    }

    private rrmdir(path: string) {
        const _this = this;
        // sanity check, so we cannot access root files
        if (!path.startsWith('/tmp/resumable_js')) return;
        if (fs.existsSync(path)) {
            fs.readdirSync(path).forEach(function(file, index){
                let curPath = path + '/' + file;
                if (fs.lstatSync(curPath).isDirectory()) {
                    // recurse
                    _this.rrmdir(curPath);
                }
                else {
                    // delete file
                    fs.unlinkSync(curPath);
                }
            });
            fs.rmdirSync(path);
        }
    }

    private mkDirByPathSync(targetDir: string, { isRelativeToScript = false } = {}) {
        // sanity check, so we cannot access root files
        if (!targetDir.startsWith('/tmp/resumable_js')) return;
        const sep = path.sep;
        const initDir = path.isAbsolute(targetDir) ? sep : '';
        const baseDir = isRelativeToScript ? __dirname : '.';
      
        return targetDir.split(sep).reduce((parentDir, childDir) => {
          const curDir = path.resolve(baseDir, parentDir, childDir);
          try {
            fs.mkdirSync(curDir);
          } catch (err) {
            if (err.code === 'EEXIST') { // curDir already exists!
              return curDir;
            }
      
            // To avoid `EISDIR` error on Mac and `EACCES`-->`ENOENT` and `EPERM` on Windows.
            if (err.code === 'ENOENT') { // Throw the original parentDir error on curDir `ENOENT` failure.
              throw new Error(`EACCES: permission denied, mkdir '${parentDir}'`);
            }
      
            const caughtErr = ['EACCES', 'EPERM', 'EISDIR'].indexOf(err.code) > -1;
            if (!caughtErr || caughtErr && curDir === path.resolve(targetDir)) {
              throw err; // Throw if it's just the last created dir.
            }
          }
      
          return curDir;
        }, initDir);
    }

    public get (req: Request & { user: User }, callback: GetCallback) {
        let chunkNumber: number = parseInt(req.param('resumableChunkNumber', 0), 10);
        let chunkSize: number = parseInt(req.param('resumableChunkSize', 0), 10);
        let totalSize: number = parseInt(req.param('resumableTotalSize', 0), 10);
        let identifier: string = req.param('resumableIdentifier', '');
        let filename: string = req.param('resumableFilename', '');
    
        if(this.validateRequest(chunkNumber, chunkSize, totalSize, identifier, filename)==ResumableStatus.Valid) {
          let chunkFilename = this.getChunkFilename(req.user.id, chunkNumber, identifier);
          fs.exists(chunkFilename, function(exists){
              if(exists){
                callback(ResumableStatus.Found, chunkFilename, filename, identifier);
              } else {
                callback(ResumableStatus.NotFound, null, null, null);
              }
            });
        } else {
          callback(ResumableStatus.NotFound, null, null, null);
        }
    }

    public post (req: Request & { file: MulterFile, user: User }, callback: PostCallback) {

        const _this = this;
        let fields = req.body;
        let file = req.file;

        let chunkNumber: number = parseInt(fields['resumableChunkNumber'], 10);
        let chunkSize: number = parseInt(fields['resumableChunkSize'], 10);
        let totalSize: number = parseInt(fields['resumableTotalSize'], 10);
        let identifier: string = this.cleanIdentifier(fields['resumableIdentifier']);
        let filename: string = fields['resumableFilename'];
        let original_filename: string = fields['resumableFilename'];

        if(!file || !file.size) {
            callback(ResumableStatus.Invalid, null, null, null);
            return;
        }

        let validation = this.validateRequest(chunkNumber, chunkSize, totalSize, identifier, filename, file.size);

        if(validation == ResumableStatus.Valid) {

            const userid = req.user.id;
            const uploadFolder = this.uploadFolder;

            let chunkFilename = this.getChunkFilename(userid, chunkNumber, identifier);
            // Save the chunk (TODO: OVERWRITE)
            const chunkFilePath = path.dirname(chunkFilename);
            this.mkDirByPathSync(chunkFilePath);
            fs.rename(file.path, chunkFilename, function (){
                // Do we have all the chunks?
                let currentTestChunk = 1;
                let numberOfChunks = Math.max(Math.floor(totalSize/(chunkSize*1.0)), 1);
                const testChunkExists = function () {
                    fs.exists(_this.getChunkFilename(userid, currentTestChunk, identifier), function (exists) {
                        if (exists){
                            currentTestChunk++;
                            if (currentTestChunk>numberOfChunks) {
                                // put the file back together again
                                _this.createFileFromChunks(userid, identifier, original_filename, numberOfChunks);

                                const upload_dir = path.join(uploadFolder, userid.toString());
                                const final_filename = path.join(upload_dir, original_filename);
                                callback(ResumableStatus.Done, filename, final_filename, identifier);
                            }
                            else {
                                // Recursion
                                testChunkExists();
                            }
                        }
                        else {
                            callback(ResumableStatus.PartlyDone, filename, original_filename, identifier);
                        }
                    });
                }
                testChunkExists();
            });
        }
        else {
            callback(validation, filename, original_filename, identifier);
        }
    }

    public write (req: Request & { user: User }, identifier: string, writableStream: Response, _options?: any) {
        const options = _options || {};
        options.end = (typeof options['end'] == 'undefined' ? true : options['end']);
        const _this = this;
        // Iterate over each chunk
        const pipeChunk = function(number: number) {
            const chunkFilename = _this.getChunkFilename(req.user.id, number, identifier);
            fs.exists(chunkFilename, function(exists) {
                if (exists) {
                    // If the chunk with the current number exists,
                    // then create a ReadStream from the file
                    // and pipe it to the specified writableStream.
                    let sourceStream = fs.createReadStream(chunkFilename);
                    sourceStream.pipe(writableStream, {
                        end: false
                    });
                    sourceStream.on('end', function() {
                        // When the chunk is fully streamed,
                        // jump to the next one
                        pipeChunk(number + 1);
                    });
                }
                else {
                    // When all the chunks have been piped, end the stream
                    if (options.end) writableStream.end();
                    if (options.onDone) options.onDone();
                }
            });
        }
        pipeChunk(1);
    }

    public clean (userid: number,identifier: string, _options?: any) {
        const options = _options || {};
        const _this = this;

        // Iterate over each chunk
        let pipeChunkRm = function(number: number) {
  
            let chunkFilename = _this.getChunkFilename(userid, number, identifier);
  
            //console.log('removing pipeChunkRm ', number, 'chunkFilename', chunkFilename);
            fs.exists(chunkFilename, function(exists) {
                if (exists) {
                    // console.log('exist removing ', chunkFilename);
                    fs.unlink(chunkFilename, function(err) {
                        if (err && options.onError) options.onError(err);
                    });
  
                    pipeChunkRm(number + 1);
  
                } else {
  
                    if (options.onDone) options.onDone();
  
                }
            });
        }
        pipeChunkRm(1);
    }

}