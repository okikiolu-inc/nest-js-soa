import * as amqp from 'amqplib/callback_api';

let conn: amqp.Connection = null;
let ch: amqp.ConfirmChannel = null;

let rmqUsername: string = null;
let rmqPassword: string = null;
let rmqHostname: string = null;
let rmqPort: number = null;
let WAIT: number = 0;

export enum RabbitMqQueues {
    Task = 'task_queue',
    CreateMind = 'create_mind_queue',
    ManageMind = 'manage_mind_queue'
};

export enum MindTypes {
    Standard = 'standard'
};

interface QueueMessage {};

export interface ManageMindMessage extends QueueMessage {
    ExecutionContext: string;
	UserID: number;
	MindID: number;
	Action: string;
};

export interface CreateContainerMessage extends QueueMessage {
    UserID: number;
	ModelLib: string;
	MindLabel: string;
	FinalFile: string;
	MindVersion: string;
	InputSchema: string;
	ExecutionContext: string;
};

export const initQueueValues = (hostname: string, port: number, username: string, password: string,) => {
    rmqUsername = username; 
    rmqPassword = password; 
    rmqHostname = hostname; 
    rmqPort = port; 
};

const initConnection = () => {
    return new Promise<amqp.Connection>(function(resolve, reject) {
        if (rmqUsername == null || rmqPassword == null
            || rmqHostname == null || rmqPort == null) {
            reject(new Error('RabbitMQ Credentials NOT Initialized'));
        }

        amqp.connect(`amqp://${rmqUsername}:${rmqPassword}@${rmqHostname}:${rmqPort}?heartbeat=5`, function(error0, connection) {
            if (error0) reject(error0);
            resolve(connection);
        });
    });
};

const initChannel = (connection: amqp.Connection) => {
    return new Promise<amqp.ConfirmChannel>(function(resolve, reject) {
       connection.createConfirmChannel(function(error1, channel) {
            if (error1) reject(error1);
            resolve(channel);
        });
    });
};

export const init = async () => {
    try {
        conn = await initConnection();
    }
    catch {
        retry();
        return;
    }

    conn.on('close', (err) => {
        retry();
    });

    try {
        ch = await initChannel(conn);
    }
    catch {
        retry();
        return;
    }

    // reset wait timer
    WAIT = 0;
    console.log('[AMQP] Connected!');
};

const retry = () => {
    WAIT += 1000;
    console.error(`[AMQP]: Connection Failed, Reconnecting in [WAIT: ${WAIT/1000}s]...`);
    if (WAIT < 25000) {
        setTimeout(init, WAIT);
    }
};

export const publishToQueue = async (queueName: string, data: QueueMessage) => {
    // if (conn == null || ch == null) await init();
    const msg = JSON.stringify(data);

    ch.assertQueue(queueName, {
        durable: true
    });

    ch.sendToQueue(queueName, Buffer.from(msg), {
        persistent: true,
    });
};

process.on('exit', () => {
    ch.close(() => {});
    conn.close();
    console.log(`Closing rabbitmq channel`);
});
