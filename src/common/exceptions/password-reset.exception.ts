import { ConflictException } from '@nestjs/common';

export class PasswordResetException extends ConflictException {
    constructor() {
      super('Could Not Reset Password');
    }
  }