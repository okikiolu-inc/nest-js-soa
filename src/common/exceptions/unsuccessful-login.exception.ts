import { UnauthorizedException } from '@nestjs/common';

export class UnsuccessfulLoginException extends UnauthorizedException {
    constructor() {
      super('Unsuccessful Login');
    }
  }