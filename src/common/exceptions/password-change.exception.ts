import { ConflictException } from '@nestjs/common';

export class PasswordChangeException extends ConflictException {
    constructor() {
      super('Could Not Reset Password');
    }
  }