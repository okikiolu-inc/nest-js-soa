import {
    ExceptionFilter,
    Catch,
    ArgumentsHost,
    HttpException,
} from '@nestjs/common';

import { UserExistsException } from '../exceptions/user-exists.exception';
import { UnsuccessfulLoginException } from '../exceptions/unsuccessful-login.exception';
import { PasswordResetException } from '../exceptions/password-reset.exception';
import { PasswordChangeException } from '../exceptions/password-change.exception';



import { Request, Response } from 'express';

@Catch(HttpException)
export class AuthExceptionFilter implements ExceptionFilter {
    public catch(exception: HttpException, host: ArgumentsHost) {
        if (exception == null) return;
        const ctx = host.switchToHttp();
        const response = ctx.getResponse<Response>();
        const request = ctx.getRequest<Request>();

        const status = exception.getStatus();

        if (exception instanceof UserExistsException) {
            request.flash('RegisterErrors', 'Email already Registered, please try again');
            response
            .status(status)
            .redirect('/register');
        } else if (exception instanceof UnsuccessfulLoginException) {
            request.flash('LoginErrors', 'Incorrect Email or Password, please try again');
            response
            .status(status)
            .redirect('/login');
        } else if (exception instanceof PasswordResetException) {
            request.flash('ResetPasswordErrors', 'Could not Reset Password!');
            response
            .status(status)
            .redirect('/resetpassword');
        } else if (exception instanceof PasswordChangeException) {
            request.flash('ResetPasswordErrors', 'Could not Change Password!');
            response
            .status(status)
            .redirect('/profile');
        } else {
            response.status(status).json({
                statusCode: status,
                timestamp: new Date().toISOString(),
                path: request.url,
            });
        }
    }
}