import { ExceptionFilter, Catch, ArgumentsHost, HttpException } from '@nestjs/common';
import { Request, Response } from 'express';
import { join } from 'path';

@Catch(HttpException)
export class HttpExceptionFilter implements ExceptionFilter {
    public catch(exception: HttpException, host: ArgumentsHost) {
      if (exception == null) return;
      const ctx = host.switchToHttp();
      const response = ctx.getResponse<Response>();
      const request = ctx.getRequest<Request>();
      console.log('exception.message:::', exception.message);
      const status = exception.getStatus();

  
      if (status === 404) {
        response
          .status(status)
          .sendFile(
            '404.html',
            { root: join(__dirname, '..','..','..', 'public', 'html') },
          );
      }
      else if (status === 500) {
        response
          .status(status)
          .sendFile(
            '505.html',
            { root: join(__dirname, '..', 'public', 'html') },
          );
      }
      // redirect to login page if not authorized
      // ****************************************
      // this functionality will be extended to 
      // return either json on html pages depending
      // on what kind of request it was i.e regular
      // page navigation or a REST API request
      // for now we just redirect primitively
      else if (status === 401 || status === 403) {
        response
        .status(status)
        .redirect('/login');
      }
      else {
        response.status(status).json({
          statusCode: status,
          timestamp: new Date().toISOString(),
          path: request.url,
        });
      }
    }
  }