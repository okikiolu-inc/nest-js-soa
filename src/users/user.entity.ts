import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    default: ''
  })
  name:string;

  @Column()
  email: string;

  @Column({
    default: ''
  })
  password: string;

  @Column({
    default: ''
  })
  google_token:string;

  @Column({
    default: ''
  })
  facebook_token:string;

  @Column({
    default: ''
  })
  github_token:string;

  @Column({
    default: ''
  })
  google_id:string;

  @Column({
    default: ''
  })
  facebook_id:string;

  @Column({
    default: ''
  })
  github_id:string;
}
