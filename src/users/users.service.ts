import { Injectable } from '@nestjs/common';

import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import * as bcrypt from 'bcrypt';

import { Profile as GoogleProfile } from 'passport-google-oauth';
import { Profile as FacebookProfile } from 'passport-facebook';
import { Profile as GithubProfile } from 'passport-github';

import { EmailService } from '../email/email.service';

import { User } from './user.entity';

export type User = any;

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
    private readonly emailService: EmailService,
  ) {}

  async findOne(email: string): Promise<User | undefined> {
    return await this.userRepository.findOne({
      email: email,
    });
  }

  async findOneById(id: number): Promise<User | undefined> {
    return await this.userRepository.findOne({
      id: id,
    });
  }

  async validateLocalOne(email: string, pass: string): Promise<any | undefined> {
    const user = await this.findOne(email);
    // checking if password is valid
    if (user) {
      const res = await bcrypt.compare(pass, user.password);
      if (res) {
        const { password, ...result } = user;
        return result;
      }
    }
    return null;
  }

  async createLocalOne(email: string, pass: string): Promise<any | undefined> {
    let user = await this.findOne(email);
    
    if (user != null) {
      // if user exists that is an error
      return null;
    }

    const saltRounds = 10;

    // generating a hash
    try {
      const hashedPassword : string = await new Promise((resolve, reject) => {
        bcrypt.hash(pass, saltRounds, function(err, hash) {
          if (err) reject(err);
          resolve(hash);
        });
      });

      user = new User();
      user.email = email;
      user.password = hashedPassword;
  
      // save to DB
      const savedUser = await this.userRepository.save(user);
      console.log('savedUser::', savedUser);
      const { password, ...result } = savedUser;

      return result;
    }
    catch (e) {
      // error hashing the password
      console.log(e);
      return null;
    }
  }

  async findOrCreateGoogleUser(token: string, refreshToken: string, profile: GoogleProfile): Promise<any | undefined> {
    let user = await this.userRepository.findOne({
      google_id: profile.id,
    });
    if (user == null) {
      //create the user using profile info
      user = new User();
      user.email = profile.emails[0].value;
      user.name = profile.displayName;
      user.google_id = profile.id;
      user.google_token = token;
  
      // save to DB
      const savedUser = await this.userRepository.save(user);
      const { password, ...result } = savedUser;
      return result;
    }
    else {
      const { password, ...result } = user;
      return result;
    }

  }

  async findOrCreateFacebookUser(token: string, refreshToken: string, profile: FacebookProfile): Promise<any | undefined> {
    let user = await this.userRepository.findOne({
      facebook_id: profile.id,
    });
    if (user == null) {
      //create the user using profile info
      console.log('Facebook', profile);
      user = new User();
      user.email = profile.emails[0].value;
      user.name = profile.displayName;
      user.facebook_id = profile.id;
      user.facebook_token = token;
  
      // save to DB
      const savedUser = await this.userRepository.save(user);
      const { password, ...result } = savedUser;
      return result;
    }
    else {
      const { password, ...result } = user;
      return result;
    }

    return user;
  }

  async findOrCreateGithubUser(token: string, refreshToken: string, profile: GithubProfile): Promise<any | undefined> {
    let user = await this.userRepository.findOne({
      github_id: profile.id,
    });

    if (user == null) {
      //create the user using profile info
      user = new User();
      user.email = profile.emails[0].value;
      user.name = profile.displayName;
      user.github_id = profile.id;
      user.github_token = token;
  
      // save to DB
      const savedUser = await this.userRepository.save(user);
      const { password, ...result } = savedUser;
      return result;
    }
    else {
      const { password, ...result } = user;
      return result;
    }
  }

  async findAll(): Promise<User[]> {
    return await this.userRepository.find();
  }
}