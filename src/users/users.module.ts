import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EmailService } from '../email/email.service';
import { UsersService } from './users.service';
import { User } from './user.entity';
import { UsersController } from './users.controller';


@Module({
  imports: [
    TypeOrmModule.forFeature([User]),
  ],
  providers: [
    UsersService,
    EmailService,
  ],
  exports: [UsersService],
  controllers: [UsersController],
})
export class UsersModule {}
