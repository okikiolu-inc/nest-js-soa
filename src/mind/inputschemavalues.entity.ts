import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn } from 'typeorm';

import { UserInputSchema } from './userinputschema.entity';

@Entity({name: 'inputschemavalues'})
export class InputSchemaValues {
  @PrimaryGeneratedColumn()
  id: number;

  // foreign key
  @Column()
  schemaid:number;

  @Column()
  schemavalue:string;

  @Column()
  schemaorder:number;

  @ManyToOne(type => UserInputSchema)
  @JoinColumn({ name: 'schemaid'})
  userinputschema: UserInputSchema;
}
