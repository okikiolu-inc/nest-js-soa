import { Entity, Column, PrimaryGeneratedColumn  } from 'typeorm';

@Entity({name: 'lkpStatus'})
export class lkpStatus {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  Status: string;
}