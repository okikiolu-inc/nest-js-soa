import { Entity, Column, PrimaryGeneratedColumn, OneToOne, JoinColumn } from 'typeorm';
import { UserImage } from './userimage.entity';
import { UserInputSchema } from './userinputschema.entity';
import { lkpStatus } from './lkpStatus.entity';
import { lkpMindTypes } from './lkpMindTypes.entity';
// import { UserOutputSchema } from './useroutputschema.entity';

@Entity({name: 'usermind'})
export class UserMind {
  @PrimaryGeneratedColumn()
  id: number;

  // foreign key
  @Column()
  userid:number;

  // foreign key
  @OneToOne(type => UserImage)
  @JoinColumn({ name: 'currentimageid' })
  userimage:UserImage;

  // foreign key
  @OneToOne(type => UserInputSchema)
  @JoinColumn({ name: 'inputschema' })
  userinputschema:UserInputSchema;

  @Column()
  mindurl:string;

  @Column()
  mindlabel:string;

  @Column()
  imagelabelname:string;

  @Column()
  @OneToOne(type => lkpMindTypes)
  @JoinColumn({ name: 'mindtype' })
  mindtype:lkpMindTypes;

  @Column()
  mindversion:string;

  @Column()
  @OneToOne(type => lkpStatus)
  @JoinColumn({ name: 'mindstatus' })
  mindstatus:lkpStatus;

  @Column()
  modelcreatetime:Date;

  @Column()
  modelstarttime:Date;
}