import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MindService } from './mind.service';
import { MindController } from './mind.controller';

import { ConfigModule } from '../config/config.module';
import { ConfigService } from '../config/config.service';

import { MulterModule } from '@nestjs/platform-express';

import { UserMind } from './usermind.entity';
import { UserImage } from './userimage.entity';
import { lkpStatus } from './lkpStatus.entity';

@Module({
  imports: [
    MulterModule.registerAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => ({
        dest: `${configService.multerUploadDestination}`,
      }),
      inject: [ConfigService],
    }),
    TypeOrmModule.forFeature([UserMind]),
    TypeOrmModule.forFeature([UserImage]),
    TypeOrmModule.forFeature([lkpStatus]),
  ],
  providers: [
    MindService,
    {
      provide: ConfigService,
      useValue: new ConfigService(`${process.env.NODE_ENV ? process.env.NODE_ENV : 'production'}.env`),
    }
  ],
  exports: [MindService],
  controllers: [MindController]
})
export class MindModule {}
