import { Test, TestingModule } from '@nestjs/testing';
import { MindController } from './mind.controller';

describe('Mind Controller', () => {
  let controller: MindController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MindController],
    }).compile();

    controller = module.get<MindController>(MindController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
