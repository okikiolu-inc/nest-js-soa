import { Injectable } from '@nestjs/common';

import { InjectRepository } from '@nestjs/typeorm';
import { Repository, FindManyOptions, FindOneOptions, Not } from 'typeorm';

import { UserMind } from './usermind.entity';
import { UserImage } from './userimage.entity';
import { lkpStatus } from './lkpStatus.entity';

@Injectable()
export class MindService {

    constructor(
        @InjectRepository(UserMind)
        private readonly userMindRepository: Repository<UserMind>,
        @InjectRepository(UserImage)
        private readonly userImageRepository: Repository<UserImage>,
        @InjectRepository(lkpStatus)
        private readonly lkpStatusRepository: Repository<lkpStatus>,
    ) {}

    async findAllUserMinds(userid: number): Promise<UserMind[] | undefined> {
        const options: FindManyOptions<UserMind> = {
            relations: [
                'userimage',
                'userimage.modeltype',
                'mindtype',
                'mindstatus',
            ],
            where: {
                userid: userid,
            }
        };

        const minds = await this.userMindRepository.find(options);

        return minds;
    }

    async findOneMind(userid: number, mindid: number): Promise<UserMind | undefined> {
        const options: FindOneOptions<UserMind> = {
            relations: [
                'userimage',
                'userimage.modeltype',
                'mindtype',
                'mindstatus',
                'userinputschema',
                'userinputschema.schemavalues',
            ],
            where: {
                id: mindid,
                userid: userid,
            }
        };

        const mind = await this.userMindRepository.findOne(options);

        return mind;
    }

    // find the N most recent minds
    async findNMostRecentMinds(userid: number, n: number): Promise<UserMind[] | undefined> {

        const lkpOptions: FindOneOptions<lkpStatus> = {
            where: {
                Status: 'DELETING',
            }
        };
        const deleteStatus  = await this.lkpStatusRepository.findOne(lkpOptions);
        if (deleteStatus == null) return [];

        const options: FindManyOptions<UserMind> = {
            relations: [
                'userimage',
                'userimage.modeltype',
                'mindtype',
                'mindstatus',
            ],
            where: {
                userid: userid,
                mindstatus: Not(deleteStatus.id),
            },
            order: {
                modelcreatetime: 'DESC',
            },
            take: n,
        };

        const minds = await this.userMindRepository.find(options);

        return minds;
    }


    async findAllUserImages(userid: number): Promise<UserImage[] | undefined> {
        return await this.userImageRepository.find({
            userid: userid,
        });
    }

    async setMindDeleting(userid: number, mindid: number) {
        const options: FindOneOptions<UserMind> = {
            where: {
                id: mindid,
                userid: userid,
            }
        };

        const usermind  = await this.userMindRepository.findOne(options);
        if (usermind == null) return;

        const lkpOptions: FindOneOptions<lkpStatus> = {
            where: {
                Status: 'DELETING',
            }
        };
        const deleteStatus  = await this.lkpStatusRepository.findOne(lkpOptions);
        if (deleteStatus == null) return;

        usermind.mindstatus = deleteStatus;

        this.userMindRepository.save(usermind);
    }
}
