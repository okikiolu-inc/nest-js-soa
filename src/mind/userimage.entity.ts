import { Entity, Column, PrimaryGeneratedColumn, OneToOne, JoinColumn } from 'typeorm';
import { lkpModelType } from './lkpModelType.entity';

@Entity({name: 'userimage'})
export class UserImage {
  @PrimaryGeneratedColumn()
  id: number;

  // foreign key
  @Column()
  userid:number;

  @Column()
  parentmindid:number;

  @Column()
  imagename:string;

  @Column()
  imagefilename:string;

  @OneToOne(type => lkpModelType)
  @JoinColumn({ name: 'modeltype' })
  modeltype:lkpModelType;

  @Column()
  version:string;
}
