import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { OutputSchemaValues } from './outputschemavalues.entity';

@Entity({name: 'useroutputschema'})
export class UserOutputSchema {
  @PrimaryGeneratedColumn()
  id: number;

  // foreign key
  @Column()
  userid:number;

  @OneToMany(type => OutputSchemaValues, outputschemavalues => outputschemavalues.schemaid)
  schemavalues: OutputSchemaValues[];
}
