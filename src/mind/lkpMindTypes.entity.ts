import { Entity, Column, PrimaryGeneratedColumn  } from 'typeorm';

@Entity({name: 'lkpMindTypes'})
export class lkpMindTypes {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  MindType: string;
}