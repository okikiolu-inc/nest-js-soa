import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';

import { UserOutputSchema } from './useroutputschema.entity';

@Entity({name: 'outputschemavalues'})
export class OutputSchemaValues {
  @PrimaryGeneratedColumn()
  id: number;

  // foreign key
  @Column()
  schemaid:number;

  @Column()
  schemavalue:string;

  @Column()
  schemaorder:number;

  @ManyToOne(type => UserOutputSchema, useroutputschema => useroutputschema.schemavalues)
  useroutputschema: UserOutputSchema;
}
