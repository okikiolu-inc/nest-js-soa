import { Entity, Column, PrimaryGeneratedColumn  } from 'typeorm';

@Entity({name: 'lkpModelType'})
export class lkpModelType {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  ModelType: string;
}