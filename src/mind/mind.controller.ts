import {
    Controller,
    Get,
    Post,
    Render,
    Req,
    Res,
    UseGuards,
    Param,
    ParseIntPipe,
    UseInterceptors,
    UploadedFile,
} from '@nestjs/common';

import { Request, Response } from 'express';

import { FileInterceptor } from '@nestjs/platform-express';

import * as crypto from 'crypto';

import { AuthenticatedGuard } from '../auth/guard/authenticated.guard';

import { Resumable, MulterFile, ResumableStatus } from '../common/libs/resumable';

import {
  publishToQueue,
  RabbitMqQueues,
  ManageMindMessage,
  CreateContainerMessage,
} from '../common/libs/rabbitmq';

import { User } from '../users/user.entity';
import { UserMind } from './usermind.entity';

import { MindService } from './mind.service';
import { ConfigService } from '../config/config.service';



@Controller('minds')
export class MindController {
    private resumable: Resumable;

    constructor (
      private readonly configService: ConfigService,
      private readonly mindService: MindService,
    ) {
      this.resumable = new Resumable(
        configService.tempUploadDestination,
        configService.uploadDestination,
      );
    }

    @UseGuards(AuthenticatedGuard)
    @Get('new')
    @Render('pages/dashboards/minds_new')
    getMindsNew(@Req() req, @Res() res) {
      return {
        page: 'minds',
        title: 'Minds',
        _csrf: res.locals._csrf,
        user: req.user,
        show_header: true,
        header_strong: 'New',
        header_normal: ' Mind',
        header_subtitle: 'Deploy a new Mind',
      };
    }

    @UseGuards(AuthenticatedGuard)
    @Get()
    @Render('pages/dashboards/minds')
    async getMinds(@Req() req, @Res() res) {

      const userid: number = req.user.id;
      const userMinds = await this.mindService.findAllUserMinds(userid);

      return {
        page: 'minds',
        title: 'Minds',
        _csrf: res.locals._csrf,
        user: req.user,
        userMinds: userMinds,
        show_header: true,
        header_strong: 'Deployed',
        header_normal: ' Minds',
        header_subtitle: 'Here is an overview of your deployed minds',
      };
    }

    @UseGuards(AuthenticatedGuard)
    @Get(':id/update')
    @Render('pages/dashboards/minds_update')
    async getUpdateMind(@Param('id', new ParseIntPipe()) id, @Req() req, @Res() res) {
      const userid: number = req.user.id;
      const mind = await this.mindService.findOneMind(userid, id);

      return {
        page: 'minds',
        title: 'Mind Overview',
        mind: mind,
        _csrf: res.locals._csrf,
        user: req.user,
      };
    }

    @UseGuards(AuthenticatedGuard)
    @Post(':id/update')
    @Render('pages/dashboards/minds_update')
    async postUpdateMind(@Param('id', new ParseIntPipe()) id, @Req() req, @Res() res) {
      const userid: number = req.user.id;
      const mind = await this.mindService.findOneMind(userid, id);

      return {
        page: 'minds',
        title: 'Mind Overview',
        mind: mind,
        _csrf: res.locals._csrf,
        user: req.user,
      };
    }

    @UseGuards(AuthenticatedGuard)
    @Get(':id')
    @Render('pages/dashboards/minds_detail')
    async getMindsDetail(@Param('id', new ParseIntPipe()) id, @Req() req, @Res() res) {
      const userid: number = req.user.id;
      const mind = await this.mindService.findOneMind(userid, id);

      return {
        page: 'minds',
        title: 'Mind Update',
        mind: mind,
        _csrf: res.locals._csrf,
        user: req.user,
      };
    }

    @UseGuards(AuthenticatedGuard)
    @Post(':id/stop_mind')
    async stopMind(@Param('id', new ParseIntPipe()) id, @Req() req, @Res() res: Response) {
      const userid: number = req.user.id;

      const queue = RabbitMqQueues.ManageMind;
      const data:ManageMindMessage = {
          ExecutionContext: req.cookies['_csrf'],
          UserID: userid,
          MindID: id,
          Action: 'Stop',
      };
      await publishToQueue(queue, data);

      res.sendStatus(200);
    }

    // restartMind - start a previously stopped mind
    @UseGuards(AuthenticatedGuard)
    @Post(':id/restart_mind')
    async restartMind(@Param('id', new ParseIntPipe()) id, @Req() req, @Res() res: Response) {
      const userid: number = req.user.id;

      const queue = RabbitMqQueues.ManageMind;
      const data:ManageMindMessage = {
          ExecutionContext: req.cookies['_csrf'],
          UserID: userid,
          MindID: id,
          Action: 'Restart',
      };
      await publishToQueue(queue, data);

      res.sendStatus(200);
    }

    @UseGuards(AuthenticatedGuard)
    @Post(':id/delete_mind')
    async deleteMind(@Param('id', new ParseIntPipe()) id, @Req() req, @Res() res: Response) {
      const userid: number = req.user.id;

      // set mind to deleting
      await this.mindService.setMindDeleting(userid, id);

      const queue = RabbitMqQueues.ManageMind;
      const data:ManageMindMessage = {
          ExecutionContext: req.cookies['_csrf'],
          UserID: userid,
          MindID: id,
          Action: 'Delete',
      };
      await publishToQueue(queue, data);

      res.sendStatus(200);
    }

    @UseGuards(AuthenticatedGuard)
    @Get('upload')
    uploadFileGet(@Req() req: Request & { user: User }, @Res() res: Response) {
        this.resumable.get(req, function(status, filename, original_filename, identifier) {
            res.send((status == ResumableStatus.Found ? 200 : 404)).send(status);
        });
    }

    @UseGuards(AuthenticatedGuard)
    @Post('upload')
    @UseInterceptors(FileInterceptor('file'))
    uploadFilePost(@Req() req: Request & { file: MulterFile, user: User }, @Res() res: Response, @UploadedFile() file: MulterFile) {
        const _this = this;
        req.file = file;

        const model_type = req.query.model_type;
        const mind_label = req.query.mind_label;
        const input_schema = req.query.input_schema;

        if (['sklearn','pytorch','onnx','tensorflow'].indexOf(model_type) < 0) {
          res.sendStatus(500);
        }

        if (mind_label == null || mind_label.trim() == '') {
          res.sendStatus(500);
        }

        this.resumable.post(req, async function(status, filename, original_filename, identifier) {
            const userid = req.user.id
            if (status == ResumableStatus.Done) {
              const final_file = original_filename;
              const queue = RabbitMqQueues.CreateMind;
              const data = {
                  ExecutionContext: req.cookies['_csrf'],
                  UserID: userid,
                  ModelLib: 'sklearn',
                  MindLabel: mind_label,
                  FinalFile: final_file,
                  InputSchema: input_schema,
                  MindVersion: '1',
              };
              await publishToQueue(queue, data);
            }

            res.send(status);
        });
    }
}
