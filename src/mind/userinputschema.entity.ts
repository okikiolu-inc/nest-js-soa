import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { InputSchemaValues } from './inputschemavalues.entity';

@Entity({name: 'userinputschema'})
export class UserInputSchema {
  @PrimaryGeneratedColumn()
  id: number;

  // foreign key
  @Column()
  userid:number;

  @OneToMany(type => InputSchemaValues, inputschemavalues => inputschemavalues.userinputschema)
  schemavalues: InputSchemaValues[];
}
