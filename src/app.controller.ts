import {
  Controller,
  Get,
  Render,
  Request,
  UseGuards,
  Response,
  Param,
  ParseIntPipe
} from '@nestjs/common';

import { AuthenticatedGuard } from './auth/guard/authenticated.guard';

import { AppService } from './app.service';
import { MindService } from './mind/mind.service';
import { publishToQueue } from './common/libs/rabbitmq';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private readonly mindService: MindService,
  ) {}

  @UseGuards(AuthenticatedGuard)
  @Get()
  @Render('pages/home')
  async root(@Request() req, @Response() res) {

    const userid: number = req.user.id;
    const userMinds = await this.mindService.findNMostRecentMinds(userid, 4);

    return {
      page: 'home',
      title: 'Home',
      _csrf: res.locals._csrf,
      userMinds,
    };
  }

  @Get('login')
  login(@Request() req, @Response() res) {
    const loginErrors = req.flash('LoginErrors');
    res.render(
      'pages/auth/login',
      {
        layout: 'auth',
        body_class: 'min-h-fullscreen bg-img center-vh p-20',
        body_style: 'background-image: url(../assets/img/bg/2.jpg);',
        data_overlay: '7',
        title: 'Sign In',
        _csrf: res.locals._csrf,
        loginErrors,
      }
    );
  }

  @Get('register')
  register(@Request() req, @Response() res) {
    const registerErrors = req.flash('RegisterErrors');
    res.render(
      'pages/auth/register',
      {
        layout: 'auth',
        body_class: 'min-h-fullscreen bg-img center-vh p-20',
        body_style: 'background-image: url(../assets/img/bg/2.jpg);',
        data_overlay: '7',
        title: 'Register',
        _csrf: res.locals._csrf,
        registerErrors,
      }
    );
  }

  @Get('resetpassword')
  resetpassword(@Request() req, @Response() res) {
    const resetPasswordErrors = req.flash('ResetPasswordErrors');
    res.render(
      'pages/auth/reset_pass',
      {
        layout: 'auth',
        body_class: 'min-h-fullscreen bg-img center-vh p-20',
        body_style: 'background-image: url(../assets/img/bg/2.jpg);',
        data_overlay: '7',
        title: 'Reset Your Password',
        _csrf: res.locals._csrf,
        resetPasswordErrors,
      }
    );
  }

  @UseGuards(AuthenticatedGuard)
  @Get('profile')
  @Render('pages/user/user_profile')
  getProfile(@Request() req, @Response() res) {
    const user = req.user;

    return {
      page: 'profile',
      title: 'Profile',
      _csrf: res.locals._csrf,
      user,
      show_header: true,
      header_strong: 'Profile',
      header_normal: ' page',
      header_subtitle: 'this is the profile page',
    };
  }

  @UseGuards(AuthenticatedGuard)
  @Get('billing')
  @Render('pages/user/billing')
  getBilling(@Request() req, @Response() res) {
    return {
      page: 'billing',
      title: 'Billing',
      _csrf: res.locals._csrf,
      user: req.user,
      show_header: true,
      header_strong: 'Billing',
      header_normal: ' page',
      header_subtitle: 'this is the billing page',
    };
  }

  @UseGuards(AuthenticatedGuard)
  @Get('settings')
  @Render('pages/user/settings')
  getSettings(@Request() req, @Response() res) {
    return {
      page: 'settings',
      title: 'Settings',
      _csrf: res.locals._csrf,
      user: req.user,
      header_strong: '',
      header_normal: '',
      header_subtitle: '',
    };
  }

  @UseGuards(AuthenticatedGuard)
  @Get('metrics')
  @Render('pages/dashboards/metrics')
  getMetrics(@Request() req, @Response() res) {
    return {
      page: 'metrics',
      title: 'Metrics',
      _csrf: res.locals._csrf,
      user: req.user,
      show_header: true,
      header_strong: 'Metrics',
      header_normal: '',
      header_subtitle: 'Here are your mind metrics',
    };
  }

  @UseGuards(AuthenticatedGuard)
  @Get('testing')
  @Render('pages/dashboards/testing')
  getTesting(@Request() req, @Response() res) {
    return {
      page: 'testing',
      title: 'Testing',
      _csrf: res.locals._csrf,
      user: req.user,
      header_strong: 'Testing',
      header_normal: '',
      header_subtitle: 'Here is an overview of your current tests',
    };
  }

  @UseGuards(AuthenticatedGuard)
  @Get('events')
  @Render('pages/dashboards/events')
  getEvents(@Request() req, @Response() res) {
    return {
      page: 'events',
      title: 'Events',
      _csrf: res.locals._csrf,
      user: req.user,
      header_strong: 'Events',
      header_normal: '',
      header_subtitle: 'Here is an overview of your current tests',
    };
  }
}
