import { Injectable } from '@nestjs/common';
import { MailerService } from '@nest-modules/mailer';
import { User } from '../users/user.entity';

@Injectable()
export class EmailService {
    constructor(
        private readonly mailerService: MailerService,
    ) {}

    async testMail (user: User) : Promise<any | undefined> {
        return await this
            .mailerService
            .sendMail({
                to: 'test@collabr.ai',
                from: 'noreply@collabr.ai',
                subject: 'Testing Nest Mailermodule with template ✔',
                template: 'welcome', // The `.pug` or `.hbs` extension is appended automatically.
                context: {  // Data to be sent to template engine.
                    code: 'cf1a3f828287',
                    username: 'john doe',
                },
            });
    }

    async sendUserSignUp (user: User) : Promise<any | undefined> {
        return await this
            .mailerService
            .sendMail({
                to: 'test@collabr.ai',
                from: 'noreply@collabr.ai',
                subject: 'Testing Nest Mailermodule with template ✔',
                template: 'welcome', // The `.pug` or `.hbs` extension is appended automatically.
                context: {  // Data to be sent to template engine.
                    code: 'cf1a3f828287',
                    username: 'john doe',
                },
            });
    }

    async sendPasswordReset (user: User) : Promise<any | undefined> {
        return await this
            .mailerService
            .sendMail({
                to: 'test@collabr.ai',
                from: 'noreply@collabr.ai',
                subject: 'Testing Nest Mailermodule with template ✔',
                template: 'welcome', // The `.pug` or `.hbs` extension is appended automatically.
                context: {  // Data to be sent to template engine.
                    code: 'cf1a3f828287',
                    username: 'john doe',
                },
            });
    }

    async sendPasswordChange (user: User) : Promise<any | undefined> {
        return await this
            .mailerService
            .sendMail({
                to: 'test@collabr.ai',
                from: 'noreply@collabr.ai',
                subject: 'Testing Nest Mailermodule with template ✔',
                template: 'welcome', // The `.pug` or `.hbs` extension is appended automatically.
                context: {  // Data to be sent to template engine.
                    code: 'cf1a3f828287',
                    username: 'john doe',
                },
            });
    }

    async sendBillingInfoUpdate (user: User) : Promise<any | undefined> {
        return await this
            .mailerService
            .sendMail({
                to: 'test@collabr.ai',
                from: 'noreply@collabr.ai',
                subject: 'Testing Nest Mailermodule with template ✔',
                template: 'welcome', // The `.pug` or `.hbs` extension is appended automatically.
                context: {  // Data to be sent to template engine.
                    code: 'cf1a3f828287',
                    username: 'john doe',
                },
            });
    }

    async sendBillingInvoice (user: User) : Promise<any | undefined> {
        return await this
            .mailerService
            .sendMail({
                to: 'test@collabr.ai',
                from: 'noreply@collabr.ai',
                subject: 'Testing Nest Mailermodule with template ✔',
                template: 'welcome', // The `.pug` or `.hbs` extension is appended automatically.
                context: {  // Data to be sent to template engine.
                    code: 'cf1a3f828287',
                    username: 'john doe',
                },
            });
    }
}
