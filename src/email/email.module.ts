import { Module } from '@nestjs/common';
import { HandlebarsAdapter, MailerModule } from '@nest-modules/mailer';

import { ConfigModule } from '../config/config.module';
import { ConfigService } from '../config/config.service';
import { EmailService } from './email.service';

@Module({
    imports: [
        MailerModule.forRootAsync({
            imports: [ConfigModule],
            useFactory: async (configService: ConfigService) => {
                // configService.smtpUser
                // configService.smtpPassword
                return {
                    transport: 'smtps://user@domain.com:pass@smtp.domain.com',
                    defaults: {
                        from:'"Collabr.ai" <help@collabr.ai>',
                    },
                    template: {
                        dir: __dirname + '/templates',
                        adapter: new HandlebarsAdapter(), // or new PugAdapter()
                        options: {
                            strict: true,
                        },
                    },
                };
            },
            inject: [ConfigService], 
          }),
    ],
    providers: [
        {
            provide: ConfigService,
            useValue: new ConfigService(`${process.env.NODE_ENV ? process.env.NODE_ENV : 'production'}.env`),
        },
        EmailService
    ],
    exports: [
        EmailService
    ],
})
export class EmailModule {}
