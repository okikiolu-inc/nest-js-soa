import {
    Controller,
    Get,
    Post,
    Res,
    UseGuards,
    Req,
    UseFilters,
} from '@nestjs/common';

import {
    Response,
    Request,
} from 'express';

import { AuthenticatedGuard } from '../auth/guard/authenticated.guard';

@Controller('billing')
export class BillingController {
    constructor() {}

    @UseGuards(AuthenticatedGuard)
    @Post('update')
    update(@Req() req, @Res() res: Response) {
        res.sendStatus(200);
    }
}
