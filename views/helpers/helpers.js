module.exports = {
  ifeq: function(a, b, options){
    if (a === b) {
      return options.fn(this);
    }
    return options.inverse(this);
  },
  if: function(a, options) {
    if (a) {
      return options.fn(this);
    }
    return options.inverse(this);
  },
  ifcond: function(v1, operator, v2, options) {
    switch (operator) {
      case '==':
        return (v1 == v2) ? options.fn(this) : options.inverse(this);
      case '===':
        return (v1 === v2) ? options.fn(this) : options.inverse(this);
      case '!=':
        return (v1 != v2) ? options.fn(this) : options.inverse(this);
      case '!==':
        return (v1 !== v2) ? options.fn(this) : options.inverse(this);
      case '<':
        return (v1 < v2) ? options.fn(this) : options.inverse(this);
      case '<=':
        return (v1 <= v2) ? options.fn(this) : options.inverse(this);
      case '>':
        return (v1 > v2) ? options.fn(this) : options.inverse(this);
      case '>=':
        return (v1 >= v2) ? options.fn(this) : options.inverse(this);
      case '&&':
        return (v1 && v2) ? options.fn(this) : options.inverse(this);
      case '||':
        return (v1 || v2) ? options.fn(this) : options.inverse(this);
      default:
        return options.inverse(this);
    }
  },
  elapsedtime: function(timestamp) {
    var myDate = new Date(timestamp);
    var nowDate = new Date();
    var duration = nowDate.getTime() - myDate.getTime();
    var seconds = duration / 1000;
    var d = Math.floor(seconds / (3600*24));
    var h = Math.floor(seconds % (3600*24) / 3600);
    var m = Math.floor(seconds % 3600 / 60);
    var s = Math.floor(seconds % 60);
    var timestring = "";

    if (s > 0) {
      timestring = s + ((s == 1) ? " Sec " : " Secs ");
    }

    if (m > 0) {
      timestring = m + ((m == 1) ? " Min " : " Mins ") + timestring;
    }

    if (h > 0) {
      timestring = h + ((h == 1) ? " Hr " : " Hrs ") + timestring;
    }

    if (d > 0) {
      timestring = d + ((d == 1) ? " Day " : " Days ") + timestring;
    }

    return timestring;
  },
}